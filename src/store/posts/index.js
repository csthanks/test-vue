import axios from "axios";

function shuffle(array) {
  let currentIndex = array.length,
    temporaryValue,
    randomIndex;

  while (0 !== currentIndex) {
    // Выбираем оставшиеся элементы...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // Меняем местами с текущим элементом
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

export const posts = {
  namespaced: true,
  state: () => ({
    topUserID: {},
    posts: [],
    error: false,
    query: 0,
  }),
  actions: {
    async fetchPosts({ state, commit }) {
      try {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/posts"
        );
        // Записываем перемешанный в массив и отсортируем
        const shuf = shuffle(response.data)
          .slice(0, state.query)
          .sort((a, b) => a.userId - b.userId);
        // Создаем массив hash со всеми id
        const hash = shuf.map((v) => v.userId);
        const obj = {};
        hash.forEach((v) => (!obj[v] ? (obj[v] = 1) : obj[v]++));
        const res = Object.keys(obj)
          .map((v) => [
            Object.assign(
              {},
              shuf.find((user) => user.userId == v)
            ),
            obj[v],
          ])
          .sort((a, b) => b[1] - a[1]);
        commit("setPosts", shuf);
        commit("setTopUser", { top: res[0][0].userId, length: res[0][1] });
      } catch (e) {
        // eslint-disable-next-line
        console.log(e);
      }
    },
  },
  mutations: {
    updateQuery(state, query) {
      state.query = query;
    },
    setPosts(state, posts) {
      state.posts = posts;
    },
    setError(state, error) {
      state.error = error;
    },
    setTopUser(state, topUserID) {
      state.topUserID = topUserID;
    },
  },
  getters: {},
};
